<?php

use Faker\Generator as Faker;

$factory->define(App\Categoria::class, function (Faker $faker) {
    return [
        'titulo' => $faker->word,
        'status' => 'ativo',
        'descricao'=> $faker->sentence($nbWords = 6, $variableNbWords = true),
    ];
});
