<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'titulo'=>$faker->sentence($nbWords = 6, $variableNbWords = true),
        'status' => 'ativo',
        'descricao'=> $faker->text,
        'imagem' => $faker->imageUrl($width = 640, $height = 480),
    ];
});
