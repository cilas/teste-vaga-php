<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
     /**
     * Os atributos que são atribuíveis em massa.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'status',
        'descricao',
    ];

    /**
     * Protege os campos 'id', 'created_at', 'update_at' de inserções
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'update_at'];

    /**
     * Campos que são datas
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * As postagens que pertencem a categoria
     */

    public function post()
    {
        return $this->belongsToMany('App\Post', 'post_categoria', 'post_id', 'categoria_id');
    }
}
