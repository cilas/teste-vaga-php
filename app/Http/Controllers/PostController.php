<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;
use App\Categoria;
use App\Http\Requests\PostRequest;

class postController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->query) {
            $posts = Post::where("titulo","LIKE","%{$request->input('query')}%")->orWhere("descricao","LIKE","%{$request->input('query')}%")->paginate(7);
        }else {
            $posts = Post::paginate(7);
        }

        return view('post.index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = $this->getCategorias();
        return view('post.create',compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {            
        $post = new Post;
        $post->titulo = $request->titulo;
        $post->status = $request->status;
        $post->descricao = $request->descricao;

        if($request->has('imagem')){
            $file = $request->file('imagem');
            $extension = $file->getClientOriginalExtension(); // recebe a extensão da imagem
            $filename =time().'.'.$extension;
            $file->move('uploads/posts/', $filename);
            $post->imagem = 'uploads/posts/'.$filename;
        }
        $post->save();

        $post->categorias()->sync(
            $request->categorias
        );
        return redirect()->route('posts.index')->with('success', 'post criada com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $post = Post::findOrFail($id);
        return view('post.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $categorias = $this->getCategorias();
        return view('post.edit',compact('post', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, post $post)
    {
        $request->validate([
            'titulo' => 'required',
            'status' => 'required',
            'descricao' => 'required',
        ]);

        $post->titulo = $request->titulo;
        $post->status = $request->status;
        $post->descricao = $request->descricao;

        if($request->has('imagem')){
            $file = $request->file('imagem');
            $extension = $file->getClientOriginalExtension(); // recebe a extensão da imagem
            $filename =time().'.'.$extension;
            $file->move('uploads/posts/', $filename);
            $post->imagem = 'uploads/posts/'.$filename;
        }
        $post->save();

        $post->categorias()->sync(
            $request->categorias
        );

        return redirect()->route('posts.show', $post->id)
                         ->with('success','post atualizado com sucesso!')
                         ->with(['post' => $post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->route('posts.index')

                        ->with('success','post excluido com sucesso');
        
    }

    public function getCategorias(){
        $categorias = Categoria::where('status', 'ativo')->get();
        return $categorias;
    }
}
