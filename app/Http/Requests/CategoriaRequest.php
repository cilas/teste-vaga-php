<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|unique:categorias|max:200',
            'status'=> 'required',
        ];
    }

    public function messages()
        {
            return [
                'titulo.required' => 'O Título precisa ser preenchido!',
                'titulo.unique:categorias' => 'Já existe uma categoria com esse título!',
                'status.required' => 'Selecione o Status da categoria!',
            ];
        }
}
