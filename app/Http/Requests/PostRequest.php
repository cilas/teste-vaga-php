<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|unique:categorias|max:200',
            'status'=> 'required',
            'descricao'=> 'required',
        ];
    }

    public function messages()
        {
            return [
                'titulo.required' => 'O Título precisa ser preenchido!',
                'status.required' => 'Selecione o Status do post!',
                'descricao.required' => 'A postagem precisa ter uma descrição!',
            ];
        }
}
