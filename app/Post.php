<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
     /**
     * Os atributos que são atribuíveis em massa.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'status',
        'descricao',
        'imagem',
    ];

    /**
     * Protege os campos 'id', 'created_at', 'update_at' de inserções
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'update_at'];

    /**
     * Campos que são datas
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
     /**
     * As categorias que pertencem ao post
     */
    public function categorias()
    {
        return $this->belongsToMany('App\Categoria', 'post_categoria', 'post_id', 'categoria_id');
    }
}
