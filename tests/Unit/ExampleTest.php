<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->get('/')->assertStatus(200);
        $this->get('/posts/create')->assertStatus(200);
        $this->get('/posts')->assertStatus(200);
        $this->get('/categorias/create')->assertStatus(200);
        $this->get('/categorias')->assertStatus(200);
    }
}
