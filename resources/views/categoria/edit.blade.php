@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-6">Adicionar nova Categorias</div>
                        <div class="col-2">
                            <a class="btn btn-success btn-sm" href="{{ route('categorias.index') }}"> Ver todas as
                                categorias</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {{-- Mostra os erros de validação --}}
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong>Erro na validação de alguns campos.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {{-- Formulário para editar categoria --}}
                    <form action="{{ route('categorias.update', $categoria->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            {{-- campo titulo --}}
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Titulo:</strong>
                                    <input type="text" name="titulo" value="{{$categoria->titulo}}" class="form-control" placeholder="Titulo da Categoria">
                                </div>
                            </div>
                            {{-- campo status --}}
                            <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Status:</strong>
                                <select name="status" class="form-control" id="status">
                                    @foreach (['ativo', 'inativo'] as $status)
                                        <option value="{{ $status }}"
                                        @if ($status == old('status', $categoria->status))
                                            selected="selected"
                                        @endif
                                        >{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>
                            {{-- campo descrição --}}
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Descrição:</strong>
                                    <textarea class="form-control" style="height:150px" name="descricao" placeholder="Descrição">{{$categoria->descricao}}</textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
