@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-4">Categorias</div>
                        <div class="col-2">
                            <a class="btn btn-success btn-sm" href="{{ route('categorias.create') }}"> Criar nova Categoria</a>
                        </div>
                    </div>                    
                </div>

                <div class="card-body">
                    {{-- Formulário de pesquisa --}}
                    <div class="row justify-content-md-center">
                            <div  class="form-group">
                            <form action="{{ route('categorias.index') }}" method="GET" class="form-inline">
                                    <div class="form-group mx-sm-3 mb-2">
                                        <input type="text" class="form-control" name="query" placeholder="Pesquise"/>
                                    </div>
                                    <button type="submit" class="btn btn-primary mb-2">Pesquisar</button>
                                </form>
                            </div>
    
                        </div>
                    {{-- mostra mensagem quando uma categoria é criada ou atualiazada com sucesso --}}
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                            
                    <table class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Titulo</th>
                            <th scope="col">Status</th>
                            <th scope="col">Descrição</th>
                            <th scope="col">Ações </th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($categorias as $categoria)
                            <tr>
                                <th scope="row">{{$categoria->id}}</th>
                                <td>{{$categoria->titulo}} </td>
                                <td>{{$categoria->status}}</td>
                                <td>{{$categoria->descricao}}</td>
                                <td>    
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a class="btn btn-primary btn-sm" href="{{ route('categorias.show',$categoria->id) }}">Mostrar</a>
                                        <a class="btn btn-warning btn-sm" href="{{ route('categorias.edit',$categoria->id) }}">Editar</a>
                                    </div>                                      
                                    
                                </td>
                            </tr> 
                            @endforeach                                                  
                        </tbody>
                      </table>
                      <div class="row justify-content-md-center">
                            {{ $categorias->links() }}
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
