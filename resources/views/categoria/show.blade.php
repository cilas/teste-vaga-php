@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-4">Detalhe da categoria</div>
                        <div class="col-2">
                            <div class="row">
                                <div class="col-4">
                                    <a class="btn btn-success btn-sm" href="{{ route('categorias.index') }}"> Voltar</a>
                                </div>
                                <div class="col-4">
                                    <a class="btn btn-primary btn-sm" href="{{ route('categorias.edit', $categoria->id) }}"> Editar</a>
                                </div>
                            </div>                            
                        </div>
                    </div>                    
                </div>
                {{-- card-body--}}
                <div class="card-body">                        
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Título:</strong>
                                        {{ $categoria->titulo }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Status:</strong>
                                        {{ $categoria->status }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Descrição:</strong>
                                        {{ $categoria->descricao }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Data Criação:</strong>
                                            {{ $categoria->created_at->format('d/m/Y h:m:s') }}
                                        </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Data da ultima atualização:</strong>
                                            {{ $categoria->updated_at->format('d/m/Y h:m:s') }}
                                        </div>
                                </div>
                            </div>
                </div>
                {{-- end card-body --}}
            </div>
        </div>
    </div>
</div>
@endsection