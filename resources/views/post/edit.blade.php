@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-6">Editar post</div>
                        <div class="col-2">
                            <a class="btn btn-success btn-sm" href="{{ route('posts.index') }}"> Ver todos os
                                posts</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {{-- Mostra os erros de validação --}}
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong>Erro na validação de alguns campos.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {{-- Formulário para criar nova post --}}
                    <form action="{{ route('posts.update', $post->id) }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            {{-- campo titulo --}}
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Titulo:</strong>
                                    <input type="text" name="titulo" value="{{$post->titulo}}" class="form-control" placeholder="Titulo do Post">
                                    </div>
                                </div>
                                {{-- campo status --}}
                                <div class="col-xs-2 col-sm-2 col-md-2">
                                <div class="form-group">
                                    <strong>Status:</strong>
                                    <select name="status" class="form-control" id="status">
                                        @foreach (['ativo', 'inativo'] as $status)
                                            <option value="{{ $status }}"
                                            @if ($status == old('status', $post->status))
                                                selected="selected"
                                            @endif
                                            >{{ $status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                </div>
                                {{-- campo categorias --}}
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Categorias:</strong>
                                        <select multiple size="6" class="form-control" name="categorias[]">
                                            @foreach ($categorias as $categoria)
                                                <option value="{{$categoria->id}}" 
                                                  @foreach ($post->categorias as $Postcategoria)
                                                    @if ($Postcategoria->id == $categoria->id)
                                                    {{'selected="selected"'}}
                                                    @endif 
                                                  @endforeach >
                                                 {{ $categoria->titulo }}</option>               
                                            @endforeach 
                                        </select>                                        
                                    </div>
                                </div>
                                {{-- campo descrição --}}
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Descrição:</strong>
                                        <textarea id="editor" class="form-control" style="height:150px" name="descricao" placeholder="Descrição">{{$post->descricao}}</textarea>
                                    </div>
                                </div>
                                {{-- campo imagem --}}
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Imagem capa:</strong>
                                        <input type="file" name="imagem" accept="image/*" />                                                                                
                                    </div>
                                    <div>
                                        @if (file_exists($post->imagem))
                                            <strong>Imagem atual:</strong>  
                                        <img class="card-img-top" src="{{asset($post->imagem)}}" style="width:100px;"> 
                                        @else
                                            {{-- <img class="card-img-top" src="{{asset(default.png)}}">  --}}
                                        @endif 
                                    </div>
                                </div>
                                
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Atualizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
