@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        {{-- mostra mensagem quando uma post é criada ou atualiazada com sucesso --}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif 
    </div>
    <div class="row justify-content-center">            
        <div class="">
            <div class="card" style="width: 30rem;">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col">Detalhe do Post</div>
                        <div class="col">
                            <div class="row">
                                <div class="col-4">
                                    <a class="btn btn-success btn-sm" href="{{ route('posts.index') }}"> Ver todos</a>
                                </div>
                                <div class="col-4">
                                    <a class="btn btn-primary btn-sm" href="{{ route('posts.edit', $post->id) }}"> Editar</a>
                                </div>
                            </div>                            
                        </div>
                    </div>                    
                </div>
                @if (file_exists($post->imagem))
                    <img class="card-img-top" src="{{asset($post->imagem)}}"> 
                @else
                    {{-- <img class="card-img-top" src="{{asset(default.png)}}">  --}}
                @endif                
                {{-- card-body--}}
                <div class="card-body">                                       
                            <div class="row">                               
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <h3>{{ $post->titulo }}</h3>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Status:</strong>
                                        {{ $post->status }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Descrição:</strong>
                                        {{ $post->descricao }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Data Criação:</strong>
                                            {{ $post->created_at->format('d/m/Y h:m:s') }}
                                        </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Data da ultima atualização:</strong>
                                            {{ $post->updated_at->format('d/m/Y h:m:s') }}
                                        </div>
                                </div>
                            </div>
                </div>
                {{-- end card-body --}}
            </div>
        </div>
    </div>
</div>
@endsection